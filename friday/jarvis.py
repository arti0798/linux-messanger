print("start............................")
import pyttsx3
import datetime
import speech_recognition as sr
import wikipedia
import webbrowser
import os
import subprocess, sys
import smtplib
import requests
import pyjokes
# import playsound
# from pygame import mixer  # Load the popular external library


engine = pyttsx3.init('espeak')
voices = engine.getProperty('voices')
#print(voices[30].id)
engine.setProperty('voice', voices[11].id)
# engine.setProperty('voice', voices[1].id)

# engine.setProperty('voice', 'english+f1')
#engine.setProperty('voice', 'english+f2')
# engine.setProperty('voice', 'english+f3')
# engine.setProperty('voice', 'english+f4')
# engine.setProperty('voice', 'english_rp+f3') #my preference
# engine.setProperty('voice', 'english_rp+f4')

def speak(audio):
    engine.say(audio)
    engine.runAndWait()

def wishMe():
    hour = int(datetime.datetime.now().hour)
    if hour >= 0 and hour < 12:
        speak("Good Morning!")
        
    elif hour >= 12 and hour < 18:
        speak("Good Afternoon")    
        
    else:
        speak("Good Evening!")
        
    speak("I m Jarvis Mam. Please tell me how may I help you")  
   
def takeCommand():
    #it takes microphone input from user and return string output
    r = sr.Recognizer()
    print("r.....***")
    with sr.Microphone() as source:
        print("Listening........")
        r.pause_threshold  = 1
        print("Listening...222.....")
        
        r.adjust_for_ambient_noise(source)
        print('now say something')
        print(r.energy_threshold)
        print("not listing****")
        audio=r.listen(source)  
        # try:
        #     audio = r.listen(source) 
        #     print(audio)
        #     print("Listening....333....")
        # except Exception as a:
        #     print("somthing went wrong")
        #     print(a) 
        print("COMPLETED!!!!!")      
    try:
        print("Recognizing.....")
        query = r.recognize_google(audio, language='en-in')
        print(f"User said: {query}\n")  
            
    except Exception as e:
        print(e)   
        
        print("Say that again please....") 
        return "None" #not python
    return query 
         
def sendEmail(to, content): 
    try:
     server = smtplib.SMTP('smtp.gmail.com', 587)   
     server.ehlo()
     server.starttls()
     server.login('sarti0798@gmail.com', '') 
     server.sendmail('sarti0798@gmail.com', to, content)
     server.close()
    except Exception as e:
     print("Exception occur!!!!")
     print(e) 
     speak('Something went wrong, Check your internet connection')  

def emailSend(query):
    try:
        speak("WHat should I say?")
        content = takeCommand() #return in string form
        # print("Enter the emailId to whom you want to send...")
        to = input("Enter the emailId to whom you want to send...")
        # to = "hazad9999@gmail.com" 
        sendEmail(to, content)
        speak("Email has been sent!!!")
        print("mail has been sent successfully")
    except Exception as e:
        print(e)
        speak("Sorry Arti I m unable to send mail!")          
def wiki(query):
    speak('Search Wikipedia.....')
    try:
        query = query.replace("wikipedia","")
        results = wikipedia.summary(query, sentences=2)
        speak("According to Wikipedia")
        print(results)
        speak(results)
    except Exception as e:
        print("can't find")
        speak("can not find anything")  

def yout(query):
    print("in youtube checking if its working")
    try:
        speak("launching youtube for you")
        # webbrowser.open('https://www.udemy.com/payment/checkout/?boId=648120&boType=course&couponCode=free215')
       
        webbrowser.open('youtube.com')
        print("its working")
        speak("YOUTUBE SUCCESSFULLY LAUNCHED FOR YOU!!")
    except Exception as e:
        print("Something went wrong")
        print(e) 

def goo(query):
    print('.....')
    try:
        speak("launching google for you")
        webbrowser.open('https://www.google.com/search?client=firefox-b-e&q=google')
    
        print("search over")
        speak("GOGGLE SUCCESSFULLY LAUNCHED FOR YOU!!")
            # url = 'https://www.google.com'
            # webbrowser.open(url, new=0, autoraise=True)   
            
    except Exception as e:
            speak("EXCEPTION OCCURED.......Check your internet Connection")
            print("SOMETHING WENT WRONG")
                               
def song(query):
    webbrowser.open('https://www.youtube.com/watch?v=HqUeSjsYLNU&list=RDHqUeSjsYLNU&start_radio=1&t=10')
    print("SUCCESSFULL!!!!!!!!!!") 

def music(query):
    try:
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        path = '/home/arti/Music/mk.mp3'
        subprocess.call([opener, path]) 
    except Exception as e:
                speak("EXCEPTION OCCURED...CHECK THE PATH OF YOUR FILE")
                print("EXCEPTION OCCURED") 
def flow(query):
    speak("OPENING STACKOVERFLOW")
    webbrowser.open('stackoverflow.com')  
    print("SUCCESSFULL!!!!!!!!!!")

def news(query):
    main_url = " https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=4dbc17e007ab436fb66416009dfb59a8"
    open_bbc_page = requests.get(main_url).json()
    article = open_bbc_page["articles"] 
    results = []
    for ar in article: 
        results.append(ar["title"]) 
          
    for i in range(len(results)): 
          
        # printing all trending news 
        print(i + 1, results[i])
        speak(results[i])

def thanks(query):
    speak('YOU ARE WELCOME')
    # speak('HAVE A GREAT DAY')

def jokes(query):
    
    speak("here some jokes for You")
    speak(pyjokes.get_joke())


def weather(query):
    api_key = "89b2d7f866dae0825aa2262a0e5708c0"
    base_url = "http://api.openweathermap.org/data/2.5/weather?"
    speak('Which city')
    complete_url = base_url + "appid=" + api_key + "&q=" + "Pune"
    response = requests.get(complete_url)
    x = response.json() 
    if x["cod"] != "404": 
  
    # store the value of "main" 
    # key i variable y 
        y = x["main"] 
        current_temperature = y["temp"] 
        speak('CURRENT TEMPERATURE IS ')
        speak(current_temperature)
        current_pressure = y["pressure"] 
        speak('PRESSURE')
        speak(current_pressure)
        current_humidiy = y["humidity"]
        speak('HUMIDITY')
        speak(current_humidiy)
        z = x["weather"] 
        weather_description = z[0]["description"]
        speak('WEATHER DESCRIPTION')
        speak(weather_description) 
        print(" Temperature (in kelvin unit) = " +
                str(current_temperature) + 
                "\n atmospheric pressure (in hPa unit) = " +
                str(current_pressure) +
                "\n humidity (in percentage) = " +
                str(current_humidiy) +
                "\n description = " +
                str(weather_description)) 
    else: 
        print(" City Not Found ")
            
def timeh(query):
    try:
        myTime = datetime.datetime.now().strftime("%H:%M:%S")
        speak(f"The Time is {myTime}")
        print(myTime)
    except Exception as e:
        speak("EXCEPTION OCCURED...SOMETHING WENT WRONG!!")
        print(e)
        print("SOMETHING WENT WRONG")         
def download(query):  
    try:
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        path = '/root/Downloads'
        subprocess.call([opener, path])  
    except Exception as e:
        speak("EXCEPTION OCCURED......SOMETHING WENT WRONG")
        print(e)
        print("SOMETHING WENT WRONG")
    
if __name__ == '__main__':
    # wishMe()
    while True:
        query = takeCommand().lower()
        
        #Logic for executin tasks based on query
        if 'wikipedia' in query:
            pass
            # speak('Search Wikipedia.....')
            # try:
            #     query = query.replace("wikipedia","")
            #     results = wikipedia.summary(query, sentences=2)
            #     speak("According to Wikipedia")
            #     print(results)
            #     speak(results)
            # except Exception as e:
            #     print("can't find")
            #     speak("can not find anything")    
            
        elif 'youtube' in query:
            pass
            # print("in youtube checking if its working")
            # try:
            #     speak("launching youtube for you")
            #     # webbrowser.open('https://www.udemy.com/payment/checkout/?boId=648120&boType=course&couponCode=free215')
               
            #     webbrowser.open('youtube.com')
            #     print("its working")
            #     speak("YOUTUBE SUCCESSFULLY LAUNCHED FOR YOU!!")
            # except Exception as e:
            #     print("Something went wrong")
            #     print(e) 
                   
             
        # elif 'google' in query:
        #     try:
        #         speak("launching google for you")
        #         webbrowser.open('https://www.google.com/search?client=firefox-b-e&q=google')
            
        #         print("search over")
        #         speak("GOGGLE SUCCESSFULLY LAUNCHED FOR YOU!!")
        #     # url = 'https://www.google.com'
        #     # webbrowser.open(url, new=0, autoraise=True)   
            
        #     except Exception as e:
        #         speak("EXCEPTION OCCURED.......Check your internet Connection")
        #         print("SOMETHING WENT WRONG")
                
        # elif 'song' in query:
        #    webbrowser.open('https://www.youtube.com/watch?v=HqUeSjsYLNU&list=RDHqUeSjsYLNU&start_radio=1&t=10')
        #    print("SUCCESSFULL!!!!!!!!!!")    
           
        # elif 'flow' in query:
        #     speak("OPENING STACKOVERFLOW")
        #     webbrowser.open('stackoverflow.com')  
        #     print("SUCCESSFULL!!!!!!!!!!")    
            
        # elif 'music' in query:
            # sound_program = "/root/Downloads/music"
            # pick a sound file you have
            # sound_file = 'Masakali%25202.0%2520_%2520A.R.%2520Rahman%2520_%2520Sidharth%2520Malhotra%252CTara%2520Sutaria%2520_%2520Tulsi%2520K%252C%2520Sachet%2520T%2520_%2520Tanishk%2520B.mp3'
            # subprocess.call([sound_program, sound_file])
            # musicDir =  '/root/Downloads/music'  
            # songs = os.listdir(musicDir)
            # print(songs)
            # os.system(os.path.join(musicDir, songs[0])) 
            #  os.startfile(os.path.join(musicDir, songs[0]))
            # opener ="open" if sys.platform == "darwin" else "xdg-open"
            # subprocess.call([opener, musicDir])
            # os.system(os.path.join(musicDir, songs[0]))
        # elif 'music' in query:
            # mixer.init()
            # mixer.music.load('e:/LOCAL/Betrayer/Metalik Klinik1-Anak Sekolah.mp3')
            # mixer.music.play()    
        # elif 'music' in query:
        #     try:
        #         opener ="open" if sys.platform == "darwin" else "xdg-open"
        #         path = '/root/Downloads/music/mk.mp3'
        #         subprocess.call([opener, path])         
            # subprocess.call([opener, '/root/Downloads/music/mk.mp3'])   working
            
            
            #try for random using this(later)      
            #    musicDir =  '/root/Downloads/music'  
            #    songs = os.listdir(musicDir)
            #    print('************')
            #    print(songs)
            #    print('************')
            #    os.startfile(os.path.join(musicDir, songs[0]))
            #    os.fstat(os.path.join(musicDir, songs[0]))
            # except Exception as e:
            #     speak("EXCEPTION OCCURED...CHECK THE PATH OF YOUR FILE")
            #     print("EXCEPTION OCCURED")
                
        # elif 'time' in query:
        #     try:
        #         myTime = datetime.datetime.now().strftime("%H:%M:%S")
        #         speak(f"The Time is {myTime}")
        #         print(myTime)
        #     except Exception as e:
        #         speak("EXCEPTION OCCURED...SOMETHING WENT WRONG!!")
        #         print(e)
        #         print("SOMETHING WENT WRONG")      
            
        # elif 'downloads' in query:
        #     try:
        #         opener ="open" if sys.platform == "darwin" else "xdg-open"
        #         path = '/root/Downloads'
        #         subprocess.call([opener, path])  
        #     except Exception as e:
        #         speak("EXCEPTION OCCURED......SOMETHING WENT WRONG")
        #         print(e)
        #         print("SOMETHING WENT WRONG")
        # elif 'send email' in query:
        #     try:
        #         speak("WHat should I say?")
        #         content = takeCommand() #return in string form
        #         # print("Enter the emailId to whom you want to send...")
        #         to = input("Enter the emailId to whom you want to send...")
        #         # to = "hazad9999@gmail.com" 
        #         sendEmail(to, content)
        #         speak("Email has been sent!!!")
        #         print("mail has been sent successfully")
        #     except Exception as e:
        #         print(e)
        #         speak("Sorry Arti I m unable to send mail!")                 
