import smtplib
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from time import timezone
from prompt_toolkit import prompt
import getpass
import fbchat
from fbchat import Client
from pyautogui import *
from instabot import Bot
import phonenumbers
# from phonedetail import mobnumber
from phonenumbers import geocoder
from phonenumbers import carrier
from friday import jarvis
from twilio.rest import Client


your_mode = confirm(text = "Enter the Mode you want to work in",title = "Mode details:Linux-Messenger",buttons=['TERMINAL MODE','DIALOG MODE'])
if(your_mode == "TERMINAL MODE"):
	print("""-------------------------------------------------------------------------
				------------------------------------------
				|	WELCOME TO LINUX-MESSENGER    		  
				|								 
				|	BUILT BY:					 
				|					 
				|		 Arti Singh   
				|               Aishwarya Sakunde         
				|								 		
				|		Postgraduate Students at:	 		  
				|	       MODERN COLLEGE OF ARTS COMMERCE AND SCIENCE
                |   			     	PUNE, SHIVAJINAGAR.
				|			 		 
				------------------------------------------
		-------------------------------------------------------------------------""")
	print("----------------------------------------------------------------------------")
	
	
	# your_adr = input("Enter the Email-Id:")
	# pass_adr= getpass.getpass()

	#Details-Facebook Message
	# print("----------------------------------------------------------------------------")
	# print("Enter your Facebook Details")
	# print("----------------------------------------------------------------------------")
	# # your_id=str(input("Enter your username:"))
	# pass_fb=getpass.getpass()

	
	
	# -----------
	# SMS 
	# -----------
	

	exit_input = input("""If you want to exit, type "EXIT", else Press any key.""")
	while(exit_input != "EXIT"):
		print(" EMAIL [1]  |  FACEBOOK MESSAGE [2] |  SMS [3] | INSTAGRAM [4]  | LOCATION [5]  |  AUDIO [6]")

		number = int(input("Menu Selection: "))

		if(number == 1):
			your_adr = input("Enter the Email-Id:")
			pass_adr= getpass.getpass()
			rec_adr = input("Receiver's email ID: ")

			msg = MIMEMultipart()

			subj = input("Subject: ")
			msg['From'] = your_adr
			msg['To'] = rec_adr 
			msg['Subject'] = subj

			body = input("Text you want to enter: ")

			msg.attach(MIMEText(body, 'plain'))

			strings = input("Enter file address: ")
			if(strings != ''):
				attachment = open(strings, "rb")
				part = MIMEBase('application', 'octet-stream')
				part.set_payload((attachment).read())
				encoders.encode_base64(part)
				part.add_header('Content-Disposition', "attachment; filename = %s" % strings)

			msg.attach(part)

			server = smtplib.SMTP('smtp.gmail.com', 587)
			server.starttls()
			server.login(your_adr, pass_adr)
			text = msg.as_string()
			server.sendmail(your_adr, rec_adr, text)
			server.quit()	
			print("MAIL SENT SUCCESSFULLY!!")

			exit_input = input("""If you want to exit, type "EXIT", else Press any key.""")

		#instachat
		elif(number == 4):
			bot = Bot()
			username = input("Enter your instagram username and password ")
			paswprd = getpass.getpass()
			bot.login(username=username, password=paswprd)


			######  upload a picture #######
			#bot.upload_photo("yoda.jpg", caption="biscuit eating baby")

			######  follow someone #######
			#bot.follow("elonrmuskk")
   
   			######  unfollow someone #######
			#bot.unfollow("elonrmuskk")

			######  send a message #######
			#bot.send_message("msg sent from terminal", 'artisingh.07')
			friendUser = input("Enter your friend username to message:")
			messageInfo = input("Enter message for your friend:")
			bot.send_message(messageInfo, friendUser)
   
			######  send a message in grp #######
			#bot.send_message("msg sent from terminal", ['artisingh.07','pooja36phad'])
			# num=int(input("Do you want to send a group message/t [1:Yes],[0:No]"))
			# if (num == 1):
			# 	name=[]
			# 	n=input('How Many username you want to enter:')
			# 	for x in range(n):
			# 			usern=input("Enter the username:")	
			# 			name.append(usern)
			# 	mess=input("Enter message here:")
			# 	bot.send_message(mess,name)
			# 	print('MESSAGE SENT SUCCESSFULLY')
   
			#### to unfollow everyone ####
   
			#bot.unfollow_everyone()

			######  get follower info #######
			my_followers = bot.get_user_followers(username)
			for follower in my_followers:
       				print(follower)
				   #print(bot.get_user_info(follower))
					#followers=bot.get_user_info(follower.username)
					#print(followers)




		#fb chat
		elif(number == 2):
			print("*U*U*U*U*U*U*U*U*U*U*************")
			client=fbchat.Client("","")
			print("*U*U*U*U*U*U*U*U*U*U*************")
			print("Type your friend's name:")
			fname=str(input("Enter friend's name:"))
			friends=client.getUsers(fname)

			friend=friends[0]
			messagetosend=str(input("Message to send:"))
			sent=client.send(friend.uid,messagetosend)
			if sent:
				print("Message sent successfully!!")
			else:
				print("Message Sending Failed!")
    
    
		#---------------
		# SMS
		
		elif(number == 3):
			ACCOUNT_SID = input("Account SID: ") # <auth sid>
			
			AUTH_TOKEN = input("Account Token: ") # <auth token>
			
			fromNumber = str(input("Enter your number: "))
			
			client = Client(ACCOUNT_SID, AUTH_TOKEN)
			# Input details
			ToNumber = str(input("Enter the number you want to send SMS: "))
			bodyText = str(input("Enter text you want to enter: "))
			client.messages.create(
			to = '+91' + ToNumber,
			from_ = '+' + fromNumber , #<fromNumber>
			body = bodyText,
			)
  
		#---------------
		#trace location
		elif(number == 5):
			mobnumber = input("Enter your number : \t")
			countryCode = "+91"
			mobnumber = countryCode + mobnumber
			ch_number = phonenumbers.parse(mobnumber, "CH")
			print(geocoder.description_for_number(ch_number,"en"))
			# newnumber = phonenumbers.parse(mobnumber)
			# timeZone = timezone.time_zones_for_number(newnumber)
			# print(timeZone)

			serviceProvider = phonenumbers.parse(mobnumber,"RO")
			print(carrier.name_for_number(serviceProvider, "en"))


		elif(number == 6):
			print("____FEATURES___\n")
			print("wikipedia\nyoutube\ngoogle\nsong\nflow\ndownloads\nnews\n")
			print("time\njokes\nweather")
			jarvis.wishMe()
			while True:
				query = jarvis.takeCommand().lower()
				print("wikipedia\nyoutube\ngoogle\nsong\nflow\ndownloads\nnews\n")
				print("time\njokes\nweather\nsend email")

				if 'wikipedia' in query:
					jarvis.wiki(query)

				elif 'youtube' in query:
					jarvis.yout(query)	

				elif 'google' in query:
					jarvis.goo(query)

				elif 'song' in query:
					jarvis.song(query)

				elif 'music' in query:
					jarvis.music(query)

				elif 'flow' in query:
					jarvis.flow(query)

				elif 'time' in query:
					jarvis.timeh(query)	

				elif 'downloads' in query:	
					jarvis.download(query)

				elif 'news' in query:	
					jarvis.news(query)	

				elif 'weather' in query:
					jarvis.weather(query)

				elif 'jokes' in query:
					jarvis.jokes(query)

				elif 'send email' in query:
					jarvis.emailSend(query)	

				elif 'thanks' in query:
					jarvis.thanks(query)
            # speak('YOU ARE WELCOME')
            # speak('HAVE A GREAT DAY')
				
				
						
		# -----------
		# 		elif 'downloads' in query:	
		# 			jarvis.download(query)			
		
  		# -----------
		# SMS

		
		
		exit_input = input("""If you want to exit, type "EXIT", else Press any key.""")
  
elif(your_mode == "DIALOG MODE"):
	st_input = "Get-Started"

	if(st_input == "Get-Started"):
		alert(text = "You will be asked some information now. Follow the Instructions now.",title='Linux-Messenger',button="OK")

		your_adr = prompt(text = "Enter your Email-Id",title="Gmail Details:Linux-Messenger", default="")

		pass_adr = password(text = "Type your gmail Password",title="Gmail Details:Linux-Messenger",default="",mask="*")

		# FB DETAIL 
		your_id = prompt(text='Enter your Facebook Username', title='FB Details:Linux-Messenger', default='')
		pass_fb = password(text='Type your Facebook Password', title='Facebook Detials:Linux-Messenger', default='', mask='*')


		# ------------
		# 
		
		ACCOUNT_SID = prompt(text='Enter your Account SID', title='Twili Details:Multi-Messenger', default='')
			# ACCOUNT_SID = raw_input("Account SID: ") # <auth sid>
		AUTH_TOKEN = prompt(text='Enter your token', title='Twilio Details:Multi-Messenger', default='')
			# AUTH_TOKEN = raw_input("Account Token: ") # <auth token>
		fromNumber = prompt(text='Enter your Twilio Number', title='Twilio Details:Multi-Messenger', default='')
			# fromNumber = str(raw_input("Enter your number: "))
		exit_input = confirm(text='Do you want to continue?', title='Confirm Message:Multi-Messenger', buttons=['YES','NO'])
			# exit_input = raw_input("""If you want to exit, type "EXIT", if not, type anything else.""")
		# ------------
		# ACCOUNT_SID = prompt(text='Enter your Account SID', title='Twili Details:Linux-Messenger', default='')
		# # ACCOUNT_SID = raw_input("Account SID: ") # <auth sid>
		# AUTH_TOKEN = prompt(text='Enter your token', title='Twilio Details:Linux-Messenger', default='')
		# # AUTH_TOKEN = raw_input("Account Token: ") # <auth token>
		# fromNumber = prompt(text='Enter your Twilio Number', title='Twilio Details:Linux-Messenger', default='')
		# fromNumber = str(raw_input("Enter your number: "))


		exit_input = confirm(text='Do you want to continue?', title='Confirm Message:Linux-Messenger', buttons=['YES','NO'])
		while(exit_input != "NO"):
			number = confirm(text='Select Menu Option?', title='Menu Details:Linux-Messenger', buttons=['EMAIL','FB MESSAGE', 'SMS'])

			if(number == "EMAIL"):
					# print  "-------------------------------------------------------------------------"
					rec_adr = prompt(text="Enter receiver's email ID", title='EMAILING:Linux-Messenger', default='')
					
					msg = MIMEMultipart()
					subj = prompt(text='Subject', title='EMAILING:Linux-Messenger', default='')
					
					msg['From'] = your_adr
					msg['To'] = rec_adr
					msg['Subject'] = subj
					# print  "-------------------------------------------------------------------------"
					body = prompt(text='Body', title='EMAILING:Linux-Messenger', default='')
					
					msg.attach(MIMEText(body, 'plain'))
					# print  "-------------------------------------------------------------------------"
					ask_attach = confirm(text='Do you want to attach any files?', title='Attaching Files:Multi-Messenger', buttons=['YES','NO'])

					print("-------------------------------------------------------------------------")
					while(ask_attach == "YES"):
						strings = prompt(text='Enter file address', title = 'FILE ADDRESS:Linux-Messenger', default='')
						# strings = raw_input("Enter file address: ")
						attachment = open(strings, "rb")
						part = MIMEBase('application', 'octet-stream')
						part.set_payload((attachment).read())
						encoders.encode_base64(part)
						part.add_header('Content-Disposition', "attachment; filename = %s" % strings)

						msg.attach(part)
						# print  "-------------------------------------------------------------------------"
						ask_attach = confirm(text='Do you want to attach more files?', title='Attaching Files:Linux-Messenger', buttons=['YES','NO'])
						# print  "-------------------------------------------------------------------------"

						try:
							server = smtplib.SMTP('smtp.gmail.com', 587)
							server.starttls()
							#print "Logging in..."
							server.login(your_adr, pass_adr)
							text = msg.as_string()
							#print "Sending mail..."
							server.sendmail(your_adr, rec_adr, text)
							server.quit()
							alert(text='MAIL SENT SUCCESSFULLY. THANK YOU FOR USING LINUX-MESSENGER', title='Success:Linux-Messenger', button = 'OK')
							#print "Mail sent successfully."
							#print "-------------------------------------------------------------------------"
						except:
							alert(text='MAIL SENDING FAILED. PLEASE CHECK YOUR DETAILS ONCE AGAIN.', title='Failure:Linux-Messenger', button='OK')
							#print("Mail sending failed.")
							#print "-------------------------------------------------------------------------"

			elif(number=="FB Message"):
					client=fbchat.Client(your_id,pass_fb)
					fname=prompt(text="Enter friend's name",title='FB Messaging:Linux-Messenger',default='')
					friends=client.getUsers(fname)
					friend=friends[0]
					messagetosend=prompt(text="Enter message to send",title='FB Messaging:Linux-Messenger',default='')
					sent=client.send(friend.uid,messagetosend)
					if sent:
							alert(text="Message sent successfully.Thank you for using Linux-Messenger!",title='Success:Linux-Messenger',button='OK')
					else:
							alert(text='Message sending failed!!Please check your deatils once again.',title='Failure:Linux-Messenger',button='OK')
       
			# SMS 
			elif(number == "SMS"):
					client = Client(ACCOUNT_SID, AUTH_TOKEN)
					# Input details
					ToNumber = prompt(text="Enter receiver's number", title='SMS:Linux-Messenger', default='')
					#ToNumber = str(raw_input("Enter the number you want to send SMS: "))
					bodyText = prompt(text="Enter message to send", title='SMS:Linux-Messenger', default='')
					#bodyText = str(raw_input("Enter text you want to enter: "))
					client.messages.create(
					to = '+91' + ToNumber,
					from_ = '+' + fromNumber , #<fromNumber>
					body = bodyText,
					)

			# elif(number == "SMS"):
			# 		client = TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)
			# 		# Input details
			# 		ToNumber = prompt(text="Enter receiver's number", title='SMS:Multi-Messenger', default='')
			# 		#ToNumber = str(raw_input("Enter the number you want to send SMS: "))
			# 		bodyText = prompt(text="Enter message to send", title='SMS:Multi-Messenger', default='')
			# 		#bodyText = str(raw_input("Enter text you want to enter: "))
			# 		client.messages.create(
			# 		to = '+91' + ToNumber,
			# 		from_ = '+' + fromNumber , #<fromNumber>
			# 		body = bodyText,
			# 		)

			exit_input = confirm(text='Do You Want To Continue?', title='Confirm Message:Linux-Messenger', buttons=['YES','NO'])
					#print "-------------------------------------------------------------------------"

					#print "Sending mail process has begun!"



    # print("-----------------------------------------------------------------------------")
	



	